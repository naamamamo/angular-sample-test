import { AuthService } from './../auth.service';
import { PostService } from './../post.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-my-post',
  templateUrl: './my-post.component.html',
  styleUrls: ['./my-post.component.css']
})
export class MyPostComponent implements OnInit {

  userId:string;
  posts$:Observable<any>;

  deletePost(id:string){
    this.postService.deletePost(this.userId,id);
  }
  numLikes(id:string,Like:number){
    if(isNaN(Like)){
      Like = 1;
    } else{
      Like = Like + 1;
    }
    this.postService.updateLike(this.userId,id,Like);
  }

  constructor(private postService:PostService, public authService:AuthService) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId); 
        this.posts$ = this.postService.getPost(this.userId);        
  }
    ) 
  }
}
