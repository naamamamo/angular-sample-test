export interface Comment {
    PostId:number,
    id:number,
    name:string,
    email:string,
    body:string
}
