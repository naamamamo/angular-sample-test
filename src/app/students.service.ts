import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class StudentsService {

  studentCollection:AngularFirestoreCollection = this.db.collection('students'); 

  public getStudents(){
    this.studentCollection = this.db.collection(`students`); 
    return this.studentCollection.snapshotChanges();
} 

deteleStudent(id:string){
  this.db.doc(`students/${id}`).delete();
}

addStudent(math:number,psyc:number,pay:string, result:string, email:string){
  const student = {math:math,psyc:psyc,pay:pay,result:result,email:email}; 
  this.studentCollection.add(student);
}

  constructor(private db:AngularFirestore) { }
}