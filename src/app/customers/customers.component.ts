import { PredictionService } from './../prediction.service';
import { Customers } from './../interfaces/customers';
import { CustomersService } from './../customers.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})

export class CustomersComponent implements OnInit {
  
  customers$;
  userId:string;
  displayedColumns: string[] = ['name', 'education', 'income', 'delete','edit','predict','result'];
  customers:Customers[];
  addCustomersFormOpen = false;
  rowToEdit:number = -1; 
  customerToEdit:Customers = {id:null,name:null, education:null, income:null};

  add(customers:Customers){
    this.customersService.addCustomers(this.userId,customers.name,customers.education,customers.income); 
  }

  deleteCustomer(id:string){
    this.customersService.deleteCustomer(this.userId,id);
  }

  // update(customer:Customers){
  //   this.customersService.updateCustomer(this.userId,customer.id, customer.name, customer.education, customer.income);
  // }
    
  moveToEditState(index){
    console.log(this.customers[index].name);
    this.customerToEdit.name = this.customers[index].name;
    this.customerToEdit.education = this.customers[index].education;
    this.customerToEdit.income = this.customers[index].income;
    this.rowToEdit = index; 
  }
  updateCustomer(){
        let id = this.customers[this.rowToEdit].id;
        this.customersService.updateCustomer(this.userId,id, this.customerToEdit.name,this.customerToEdit.education,this.customerToEdit.income);
        this.rowToEdit = null;
  }

  predict(i){
    this.predictionService.predict(this.customers[i].education, this.customers[i].income).subscribe(
      res => {console.log(res);
        if(res > 0.5){
          var result = 'Will Pay';
          console.log(result)
        } else {
          var result = 'Will not Pay'
          console.log(result)
        }
      this.customers[i].result = result}
    );  
  }

  updateResult(index){
       this.customers[index].saved = true; 
       this.customersService.updateResult(this.userId,this.customers[index].id,this.customers[index].result);
    }

  constructor(private customersService:CustomersService, 
              public authService:AuthService,
              public predictionService:PredictionService) { }


  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId); 
        this.customers$ = this.customersService.getCustomers(this.userId);
        
        this.customers$.subscribe(
          docs =>{
            this.customers = [];
            for(let document of docs){
              const customer:Customers = document.payload.doc.data();
              if (customer.result){
                customer.saved = true;
              }
              customer.id = document.payload.doc.id; 
              this.customers.push(customer); 
            }
          }
        ) 
        }
      )
  }
}