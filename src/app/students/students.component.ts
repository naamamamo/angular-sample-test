import { StudentsService } from './../students.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-students',
  templateUrl: './students.component.html',
  styleUrls: ['./students.component.css']
})
export class StudentsComponent implements OnInit {

  displayedColumns: string[] = ['math', 'psyc', 'pay', 'email','result','delete'];
  students$;
  students = [];


  delete(id:string){
    this.studentsService.deteleStudent(id);
  }

  constructor(private studentsService:StudentsService) { }

  ngOnInit(): void {
    this.students$ = this.studentsService.getStudents();
        
    this.students$.subscribe(
      docs =>{
        this.students = [];
        for(let document of docs){
          const student:any = document.payload.doc.data();
          student.id = document.payload.doc.id; 
          this.students.push(student); 
        }
      }
    ) 
    }
  }