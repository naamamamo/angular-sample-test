import { Comment } from './interfaces/comment';
import { HttpClient } from '@angular/common/http';
import { Injectable, ɵisListLikeIterable } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from './interfaces/post';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  private urlPosts = "https://jsonplaceholder.typicode.com/posts/";
  private urlComments = "https://jsonplaceholder.typicode.com/comments/";

  postsCollection:AngularFirestoreCollection;
  userCollection:AngularFirestoreCollection = this.db.collection('users');

  getPosts():Observable<Post>{
    return this.http.get<Post>(this.urlPosts);
  }

  getComments():Observable<Comment>{
    return this.http.get<Comment>(this.urlComments);
  }

  addPost(userId:string,id:number,title:string,body:string){
    const posts = {id:id,title:title,body:body};
    this.userCollection.doc(userId).collection('posts').add(posts);
     }

  deletePost(Userid:string, id:string){
      this.db.doc(`users/${Userid}/posts/${id}`).delete(); 
    } 

  updateLike(userId:string,id:string,Like:number){
      this.db.doc(`users/${userId}/posts/${id}`).update(
        {
          Like:Like
        }
      )
    }

  getPost(userId){
    this.postsCollection = this.db.collection(`users/${userId}/posts`);
     return this.postsCollection.snapshotChanges().pipe(map(
      collection =>collection.map(
        document => {
          const data = document.payload.doc.data();
          data.id = document.payload.doc.id;
          return data;
                }
             )
            ))
          }
  

  constructor(private http:HttpClient,
              private db:AngularFirestore) { }
}
