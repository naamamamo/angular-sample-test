import { Comment } from './../interfaces/comment';
import { Post } from './../interfaces/post';
import { PostService } from './../post.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  posts$:Observable<Post>;
  comments$:Observable<Comment>;
  userId:string;
  message:string;
  dis:boolean = false;
  saveState = [];


  savePost(id:number,title:string,body:string){
      this.postsService.addPost(this.userId,id,title,body);
      this.message = "Saved for later viewing";
      this.dis = true;
  }

  constructor(private postsService:PostService,public authService:AuthService) { }

  ngOnInit(): void {
    this.posts$ = this.postsService.getPosts();
    this.comments$ = this.postsService.getComments();
    this.authService.user.subscribe(
      user => {
        this.userId = user.uid;
      }
    )
  }
}