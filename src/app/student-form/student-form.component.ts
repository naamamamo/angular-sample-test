import { Router } from '@angular/router';
import { AuthService } from './../auth.service';
import { StudentsService } from './../students.service';
import { LambdaService } from './../lambda.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-student-form',
  templateUrl: './student-form.component.html',
  styleUrls: ['./student-form.component.css']
})
export class StudentFormComponent implements OnInit {
  
  options:Object[] = [{id:1,value:'yes'},{id:2,value:'no'}]
  option:string;
  math:number;
  psyc:number;
  pay;
  result:string;
  email:string;

  predict(){
    this.lambdaService.predict(this.math,this.psyc,this.pay).subscribe(
      res => {console.log(res);
        if(res > 0.5){
          var result = 'Will leave';
          console.log(result)
        } else {
          var result = 'Will not leave'
          console.log(result)
        }
      this.result = result
    });  
  }

  cancel(){
    this.result=null;
  }

  add(){
    this.StudentsService.addStudent(this.math,this.psyc,this.pay,this.result,this.email);
    this.Router.navigate(['/studentsTable']);  
  }


  constructor(private lambdaService:LambdaService,
              private StudentsService:StudentsService,
              public auth:AuthService,
              private Router:Router) { }

  ngOnInit(): void {
    this.auth.getUser().subscribe(
           user => {
            this.email = user.email;
             }
              )
            }       
              }
                  
