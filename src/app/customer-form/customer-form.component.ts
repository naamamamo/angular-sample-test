import { Customers } from './../interfaces/customers';
import { Component, EventEmitter, Input, OnInit } from '@angular/core';
import { Output } from '@angular/core';

@Component({
  selector: 'customer-form',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {
  
  isError:boolean = false;
  @Input() name:string;
  @Input() education:number;
  @Input() income:number;
  @Input() id:string;
  @Output() closeForm = new EventEmitter<null>();
  @Output() update = new EventEmitter<Customers>();
  @Input() formType:string;


  updateParent(){
    let customers:Customers = {id:this.id, name:this.name, education:this.education,income:this.income};
    if(this.education<0 || this.education>24){
      this.isError = true;
    }else{
        this.update.emit(customers);
    if (this.formType == 'Add Customer'){
      this.name = null;
      this.education = null;
      this.income = null;
    }
  }
}

  tellParentToClose(){
    this.closeForm.emit();
  }

  constructor() { }

  ngOnInit(): void {
  }

}
