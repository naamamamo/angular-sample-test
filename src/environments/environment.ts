// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBiP6uYYqzg4D38T5G_lGqrZ88vUbHDA_Q",
    authDomain: "sampletest-1314e.firebaseapp.com",
    projectId: "sampletest-1314e",
    storageBucket: "sampletest-1314e.appspot.com",
    messagingSenderId: "1074436790528",
    appId: "1:1074436790528:web:9a321616810051dd1fbbe5"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
